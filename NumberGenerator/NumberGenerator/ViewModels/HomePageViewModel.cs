﻿using NumberGenerator.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NumberGenerator.ViewModels
{
    public class HomePageViewModel : BindableBase, INavigationAware
    {

        private INavigationService NavigationService;

        public ICommand GenerateBtn { get; }

        public HomePageViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
            GenerateBtn = new DelegateCommand(GenerateNum);
        }


        private void GenerateNum()
        {
            NavigationService.NavigateAsync(nameof(GenerateNumber));
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
           
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
           
        }
    }
}
