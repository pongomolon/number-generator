﻿using NumberGenerator.Models;
using NumberGenerator.Services.Base;
using NumberGenerator.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;


namespace NumberGenerator.ViewModels
{
    public class LoginPageViewModel : BindableBase
    {

        private readonly IPageDialogService _dialogService;
        private readonly INavigationService NavigationService;
        private readonly ISQLiteService _sqliteService;

        public LoginPageViewModel(INavigationService navigationService, IPageDialogService dialogService, IPageDialogService pageDialogService, ISQLiteService sqliteService)
        {
            NavigationService = navigationService;
            _sqliteService = sqliteService;
            _dialogService = dialogService;

            SubmitBtn = new DelegateCommand(Submit);
            SignupBtn = new DelegateCommand(Signup);
            
        }

        public ICommand SubmitBtn { get; }
        public ICommand SignupBtn { get; }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }
        public async void Submit()
        {
            User UID = await _sqliteService.GetUser(Username, Password);
            if (UID != null)
            {
                await NavigationService.NavigateAsync(nameof(HomePage));
            }
            else if (Username == "admin" && Password == "admin")
            {
                await NavigationService.NavigateAsync(nameof(AdminPage));
            }
            else
            {
                await _dialogService.DisplayAlertAsync("Ops.", "Incorrect Username or Password!", "OK");
            }
        }

        private void Signup()
        {
            NavigationService.NavigateAsync(nameof(RegistrationPage));
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
          
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
          
        }
    }
}
