﻿using System;
using System.Collections.Generic;
using System.Text;
using NumberGenerator.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System.Windows.Input;
using NumberGenerator.Services.Base;
using Xamarin.Forms;
using NumberGenerator.Models;
using System.Collections.ObjectModel;

namespace NumberGenerator.ViewModels
{
    public class AdminPageViewModel : BindableBase
    {
        private readonly INavigationService NavigationService;
        private readonly ISQLiteService _sqliteService;
        public List<User> _user;

        private ObservableCollection<User> userList;

        public ObservableCollection<User> Userlist
        {
            get => userList;
            set => SetProperty(ref userList, value);
        }

        public AdminPageViewModel(INavigationService navigationService, IPageDialogService dialogService, ISQLiteService sqliteService)
        {
            NavigationService = navigationService;
            _sqliteService = sqliteService;

            ShowBtn = new DelegateCommand(Show);

        }
        private async void Show()
        {
            var res = await _sqliteService.GetData();
            _user = new List<User>(res);
            Userlist = new ObservableCollection<User>(_user);
        }

        public ICommand ShowBtn { get; }


        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
      
        }
    }
}
