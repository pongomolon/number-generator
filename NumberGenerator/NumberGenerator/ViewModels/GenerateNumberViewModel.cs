﻿using NumberGenerator.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace NumberGenerator.ViewModels
{
    public class GenerateNumberViewModel : BindableBase, INavigationAware
    {

        Random random = new Random();
        


        public ICommand LogoutBtn { get; }

        public GenerateNumberViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
            LogoutBtn = new DelegateCommand(Logout);
            NumbGen = Generate(NumbGen);
        }

        private int _numbgen;
        public int NumbGen
        {
            get { return _numbgen; }
            set { SetProperty(ref _numbgen, value); }
        }

        public int Generate(int n)
        {
            Device.StartTimer(TimeSpan.FromSeconds(30), () =>
            {
                NumbGen = random.Next(100000, 1000000);
                n = NumbGen;
                return true;
            });
            return n;
        }

        private void Logout()
        {
            NavigationService.NavigateAsync(nameof(LoginPage));
        }

        private readonly INavigationService NavigationService;

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            throw new NotImplementedException();
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            throw new NotImplementedException();
        }
    }
}
