﻿using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Prism.Mvvm;
using System.Windows.Input;
using NumberGenerator.Models;
using NumberGenerator.Services;
using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Text;
using NumberGenerator.Views;
using System.Linq;
using NumberGenerator.Services.Base;

namespace NumberGenerator.ViewModels
{
    public class RegistrationPageViewModel : BindableBase
    {
        private readonly INavigationService NavigationService;
        private readonly IPageDialogService _pageDialogService;
        private readonly ISQLiteService _sqliteService;

        public RegistrationPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ISQLiteService sqliteService)
        {
            NavigationService = navigationService;
            _sqliteService = sqliteService;
            _pageDialogService = pageDialogService;

            BackBtn = new DelegateCommand(Back);
            RegisterBtn = new DelegateCommand(Register);
            
        }

        #region Bindable Command
        public ICommand BackBtn { get; }
        public ICommand RegisterBtn { get; }
        #endregion

        #region Bindable Properties
        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _cPassword;
        public string CPassword
        {
            get { return _cPassword; }
            set { SetProperty(ref _cPassword, value); }
        }

        private string _fName;
        public string FirstName
        {
            get { return _fName; }
            set { SetProperty(ref _fName, value); }
        }

        private string _mName;
        public string MiddleName
        {
            get { return _mName; }
            set { SetProperty(ref _mName, value); }
        }

        private string _lName;
        public string LastName
        {
            get { return _lName; }
            set { SetProperty(ref _lName, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        #endregion

        #region Methods
        private void Back()
        {
            NavigationService.GoBackAsync();
        }

        private async void Register()
        {
            var hasRegex = new Regex(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()]).*");
            var hasEmailReg = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$");
            int res = Password.Length - Password.Replace("a", "").Length;

            User UID = await _sqliteService.GetUsernameByUser(Username);
            if (UID != null) {
                await _pageDialogService.DisplayAlertAsync("Alert", "Username already taken.", "OK");
            }
            else if (!hasRegex.IsMatch(Password))
            {
                await _pageDialogService.DisplayAlertAsync("Alert", "Password should contain atleast 1 Number, 1 Uppercase, 1 Lowercase, 1 Non-Alphanumeric Character.", "OK");
            }
            else if (Password != CPassword)
            {
                await _pageDialogService.DisplayAlertAsync("Alert", "Password and Confirm Password not match.", "OK");
            }
            else if (res == 8)
            {
                await _pageDialogService.DisplayAlertAsync("Alert", "Password must be 8 Characters long", "OK");
            }
            else if (!hasEmailReg.IsMatch(Email))
            {
                await _pageDialogService.DisplayAlertAsync("Alert", "Email format invalid", "OK");
            }
            else
            {
                await _sqliteService.SaveData(new User
                {
                    Username = Username,
                    Password = Password,
                    CPassword = CPassword,
                    FirstName = FirstName,
                    MiddleName = MiddleName,
                    LastName = LastName,
                    Email = Email
                });
                await _pageDialogService.DisplayAlertAsync("Message", "User is saved!", "OK");
                await NavigationService.GoBackAsync();
            }
        }
        #endregion
    }
}
