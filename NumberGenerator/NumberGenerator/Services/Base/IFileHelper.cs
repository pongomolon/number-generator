﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NumberGenerator.Services.Base
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string databaseName);
    }
}
