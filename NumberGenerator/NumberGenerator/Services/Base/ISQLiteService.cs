﻿using NumberGenerator.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NumberGenerator.Services.Base
{
    public interface ISQLiteService
    {
            Task<int> SaveData(User user);
            Task<List<User>> GetData();
            Task<User> GetUser(string username, string password);
            Task<User> GetUsernameByUser(string username);
    }
}
