﻿using NumberGenerator.Models;
using NumberGenerator.Constants;
using SQLite;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using NumberGenerator.Services.Base;

namespace NumberGenerator.Services
{
    public class SQLiteService : ISQLiteService
    {
        private readonly SQLiteAsyncConnection _sqliteConnection;

        public SQLiteService()
        {
            string databasePath = DependencyService.Get<IFileHelper>().GetLocalFilePath(AppConstants.DatabaseFileName);

            _sqliteConnection = new SQLiteAsyncConnection(databasePath, AppConstants.Flags);
            _sqliteConnection.CreateTableAsync<User>();
        }

        public async Task<List<User>> GetData()
        {
            return await _sqliteConnection.Table<User>().ToListAsync();
        }

        public async Task<User> GetUser(string username, string password)
        {
            return await _sqliteConnection.Table<User>().Where(x => x.Username == username && x.Password == password).FirstOrDefaultAsync();
        }

        public async Task<int> SaveData(User user)
        {
            return await _sqliteConnection.InsertAsync(user);
        }

        public async Task<User> GetUsernameByUser(string username)
        {
            return await _sqliteConnection.Table<User>().Where(x => x.Username == username).FirstOrDefaultAsync();
        }
    }
}
