﻿using NumberGenerator.Services;
using NumberGenerator.Services.Base;
using NumberGenerator.Views;
using Prism.DryIoc;
using Prism.Ioc;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace NumberGenerator
{
    public partial class App : PrismApplication
    {
        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.NavigateAsync($"app:///{nameof(LoginPage)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<ISQLiteService, SQLiteService>();

            containerRegistry.RegisterForNavigation<LoginPage>();
            containerRegistry.RegisterForNavigation<HomePage>();
            containerRegistry.RegisterForNavigation<RegistrationPage>();
            containerRegistry.RegisterForNavigation<GenerateNumber>();
            containerRegistry.RegisterForNavigation<AdminPage>();
        }
    }
}
