﻿using SQLite;

namespace NumberGenerator.Constants
{
    public static class AppConstants
    {
        public const string DatabaseFileName = "user.db3";
        public const SQLiteOpenFlags Flags = SQLiteOpenFlags.Create | SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.SharedCache;
    }
}