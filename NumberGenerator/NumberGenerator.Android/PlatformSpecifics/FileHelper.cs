﻿using NumberGenerator.Droid.PlatformSpecifics;
using NumberGenerator.Services.Base;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace NumberGenerator.Droid.PlatformSpecifics
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string databaseName)
        {
            return Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), databaseName);
        }
    }
}